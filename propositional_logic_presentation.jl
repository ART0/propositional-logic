### A Pluto.jl notebook ###
# v0.12.17

using Markdown
using InteractiveUtils

# ╔═╡ 7946e0d0-45b8-11eb-3d85-3d2bc8493565
using Memoize

# ╔═╡ 3f153a28-445e-11eb-2e98-d115965e9c71
md"# How to teach Propositional Logic to a Computer 💻"

# ╔═╡ 602b6b24-445e-11eb-30f1-4f49734960a3
md"### **Anand Rao Tadipatri** _and_ **Dhruv Bhasin**"

# ╔═╡ 9fb63616-445e-11eb-214e-3db3daaa4284
md"##### Indian Institute of Science Education and Research (IISER), Pune"

# ╔═╡ 247443de-445f-11eb-2e6d-a5bfa9851737
md"---------------------------------------------"

# ╔═╡ 777b58d8-445f-11eb-2b47-fdd4daefea79
md"
##### Contents
- The basic set-up for evaluating a propositional formula
- Checking formal proofs of propositional logic derived in Hilbert-style deductive calculus
- _Generating_ formal proofs of simple propositional formulae 
"

# ╔═╡ fd470142-445f-11eb-05a5-d7cb6c10720a
md"---"

# ╔═╡ 2af3b9bc-4462-11eb-0f9d-f3e4ed1e988a
md"---"

# ╔═╡ 13a5b0f6-4462-11eb-3743-81e88a95eb29
md"#### The basics of propositional logic"

# ╔═╡ 172f7430-445f-11eb-23e6-97e4eb96db51
md"Defining the meta-language"

# ╔═╡ 25213860-445e-11eb-01b6-0d566af125ae
#Negation
(¬)(p::Bool)::Bool = !p

# ╔═╡ 8892e196-445e-11eb-3113-d1815e252604
#Disjunction
∨(p::Bool, q::Bool)::Bool =  p || q

# ╔═╡ f6ffa9ac-445e-11eb-152f-4dbaaad9e32a
#Conjunction
∧(p::Bool, q::Bool)::Bool = ¬(¬p ∨ ¬q)

# ╔═╡ ffd5f324-445e-11eb-1dd8-0dd2a846a650
#Implication
⟹(p::Bool, q::Bool)::Bool = ¬p ∨ q

# ╔═╡ 06093198-445f-11eb-30a2-a3c4d3c271ac
#Biconditional
⟺(p::Bool, q::Bool)::Bool = (p ⟹ q) ∧ (q ⟹ p)

# ╔═╡ 45791c6c-445f-11eb-2db2-f1567a1b9fd4
md"----"

# ╔═╡ 2d60d5a6-4460-11eb-1dc0-6b0d659c697f
md"The data-type for well-formed formulae"

# ╔═╡ 32c7a836-445f-11eb-031c-9775d9466750
#The data-type for well-formed formulae
WFF = Union{Symbol, Expr}

# ╔═╡ 4f542740-445f-11eb-3358-1f8684540652
md"---"

# ╔═╡ 5309d4ca-445f-11eb-344c-ff6dfba11977
md"The language"

# ╔═╡ 66cf1092-445f-11eb-0bf7-89ab73d35dc6
#The language
𝕃 = (:p, :q, :r)

# ╔═╡ 3422d740-4460-11eb-0653-dd60c027d677
"""
Generates all well-formed formulae of size `n` from the given set `L` by recursively joining existing propositions with `¬` and (`∨`/`⟹`).

S𝕃(n) = S𝕃(n-1) ∪ {(¬s), (s ∨ t) | s, t ∈ S𝕃(n-1)}

**Syntax:** `S𝕃(L::Vector{<:Any}, n::Int = 1, op = :∨/:⟹)::Vector{<:Any}`
"""
function S𝕃(L::Vector{<:Any}, n::Int = 1, op = :∨)
    n == 0 ?
    L :
    [
    S𝕃(L, n-1, op);
    [:(¬$s) for s ∈ S𝕃(L, n-1, op)];
    [Expr(:call, op, s, t) for s ∈ S𝕃(L, n-1, op) for t ∈ S𝕃(L, n-1, op)]
    ] |> Set |> collect
end

# ╔═╡ 1e309c6a-4460-11eb-1542-c96357ecf472
SL = S𝕃([𝕃...], 3)

# ╔═╡ 7fd80674-4460-11eb-11d9-83146bf540ee
rand(SL)

# ╔═╡ be8dbd78-4460-11eb-1641-65b1cf8113af
md"---"

# ╔═╡ d5b649ca-4460-11eb-1483-0b359ee64cba
md"Rewriting variables and evaluating expressions"

# ╔═╡ e8f8ca58-4460-11eb-09b4-e91a325e1863
"""
Rewrites a formula, replacing a given symbol with a boolean value (true / false).
"""
function rewrite!(formula::Expr, s::Symbol, v::Bool)
    for (i, e) in enumerate(formula.args)
        if e == s
            formula.args[i] = v
        elseif typeof(e) == Expr
            rewrite!(e, s, v)
        end
    end
end

# ╔═╡ 14732d9a-4461-11eb-3a59-85e4949a755c
#A demonstration of the `rewrite!` function
let f = :( (¬p ∨ (q ∧ ¬r)) ∨ (p ⟹ ¬p) )
	rewrite!(f, :p, true)
	f
end

# ╔═╡ 6a16b064-4461-11eb-09be-6f8fd0c921e5
"""
Applies a given valuation to a formula `φ`.
"""
function evaluate(φ::Expr, v::NTuple{N, Bool}, L::NTuple{N, Symbol} = 𝕃)::Bool where N
    formula = deepcopy(φ)  #copying to preserve the input formula

    for (val, s) ∈ zip(v, L)
        rewrite!(formula, s, val)
    end

    eval(formula)
end

# ╔═╡ 1e8e6354-45b7-11eb-2260-e36742dd8de2
evaluate(ϕ::Symbol, v, L = 𝕃) = evaluate(Expr(:call, (x -> x), ϕ), v, L)

# ╔═╡ 83a78aba-4461-11eb-107b-d75a1826ffb2
"""
Checks if a valuation v satisfies a given set of formulae.
"""
function evaluate(formulae::Vector{<:Any}, v::NTuple{N, Bool}, L = NTuple{N, Symbol} = 𝕃)::Bool where N
	#returns `true` iff all statements evaluate to `true`
	reduce(∧, [evaluate(formula, v, L) for formula ∈ formulae])
end

# ╔═╡ 7413d074-4461-11eb-3949-23bc47825926
#A demonstration of the `evaluate` function
let L = (:p, :q, :r)
	ϕ = :((¬r ⟹ (q ∧ p)) ⟺ (((r ∨ ¬p) ∧ (p ⟹ q)) ∨ p))
	evaluate(ϕ, (true, false, true), L)
end

# ╔═╡ 9bbe2b62-4461-11eb-35d4-99da63f0d6b6
"""
Gives the list of all valuations that satisfy the given formula.
"""
function ν(formula, L::Tuple{Vararg{Symbol}} = 𝕃)
    #the set of all possible valuations for the symbols in 𝕃
    vs = Iterators.product(ntuple(i -> (false, true), length(L))...) |> collect |> vec

    [v for v ∈ vs if evaluate(formula, v, L)]
end

# ╔═╡ aa0177fa-45fa-11eb-0ca1-07ab18c65573


# ╔═╡ a35c287c-4461-11eb-0009-fbfbe05230e2
"""
Checks if a proposition `t` is a logical consequence of a given set of propositions `S`.

In other words, this function checks if `S ⊨ t`.
"""
⊩(S::Vector{<:Any}, t::WFF, L::Tuple{Vararg{Symbol}} = 𝕃)::Bool = ν(S, L) ⊆ ν(t, L)

# ╔═╡ ac833102-4461-11eb-3bb6-cbdf7573e7b1
"""
Checks if two propositional formulae `s` and `t` are logically equivalent (`s ≡ t`).
"""
≌(s::WFF, t::WFF, L::Tuple{Vararg{Symbol}} = 𝕃)::Bool = Set(ν(t, L)) == Set(ν(s, L))

# ╔═╡ bc523d9e-4461-11eb-25b7-379ebdb6a94f
#A demonstration of the `⊩` function
let L = (:p, :q, :r)
	S = [:(p ∧ p), :(p ⟹ r)] 
	t = :(r ∨ r)
	S ⊩ t
end

# ╔═╡ f45dda24-4461-11eb-3efa-fd40641b886e
#A demonstration of the `≌` function
let L = (:p, :q, :r)
	f₁ = :(p ∨ ¬(p ⟹ q)) 
	f₂ = :(p ∧ p)
	f₁ ≌ f₂
end

# ╔═╡ 025ecf2a-4462-11eb-3b21-a5503782a1a9
md"---"

# ╔═╡ 369ba810-4462-11eb-184d-271abf7bdff0
md"---"

# ╔═╡ 3b49f416-4462-11eb-0af9-0be138384abf
md"#### Verification of formal proofs"

# ╔═╡ 6e53519a-4462-11eb-0b28-fdf3e3e2f92e
md"Hilbert-style deductive calculus"

# ╔═╡ d8b3f38e-4462-11eb-3e99-9b3d9a71e36a
md"

__*The Logical Axioms*__

**LA1.**     $s \to (t \to s)$

**LA2.**     $(s \to (t \to u)) \to ((s \to t) \to (s \to u))$

**LA3.**     $\neg \neg s \to s$

**LA4.**     $(\neg s \to \neg t) \to (t \to s)$
"

# ╔═╡ 7edba51a-4462-11eb-16f7-ab29e537faac
#The logical axioms

#These are implemented as functions which take in formulae as inputs and return tautologies with these formulae sbstituted in the appropriate locations.

begin
	LA₁(s::WFF, t::WFF)::WFF = :( $s ⟹ ($t ⟹ $s) )
	
	LA₂(s::WFF, t::WFF, u::WFF)::WFF = :( ($s ⟹ ($t ⟹ $u)) ⟹ (($s ⟹ $t) ⟹ ($s ⟹ $u)) )
	
	LA₃(s::WFF)::WFF = :( ¬¬$s ⟹ $s )
	
	LA₄(s::WFF, t::WFF)::WFF = :( (¬$s ⟹ ¬$t) ⟹ ($t ⟹ $s) )
	
end

# ╔═╡ 7ac112fe-4468-11eb-17c9-c7f945429086
md"
__*Rule of Inference -* Modus Ponens__

$s \to t$

$s$

---

$t$

"

# ╔═╡ b8889130-4462-11eb-1fdf-433cb2015f17
#Modus Ponens

"""
Checks if the formula `s` is of the form `t ⟹ u`, where `t` is the second input to the function and `u` is the conclusion, which is returned.
"""
MP(s::WFF, t::WFF)::WFF = 
s.args[begin] == :⟹ && (s.args[begin+1] == t) ? 
s.args[end] : :missing

# ╔═╡ 315ca820-4469-11eb-23f4-37a7ecac25bf
md"
__*Sequents*__

For $S \subseteq S\scr{L}$, and $s, t \in S\scr{L}$

**(NLA).**  If $s \in S$ then $S\vdash s$.

**(LA).** If $t$ is a logical axiom then $S \vdash t$.

**(MP).** If $S \vdash (s \to t)$ and $S \vdash s$ are valid sequents then $S \vdash t$.

"

# ╔═╡ fa99e56a-4469-11eb-1a4b-5dd6844513c2
#Non-Logical Axioms

#Picks the `n`-th element from the list `S` of assumptions.
NLA(S::Vector{<:Any}, n::Int)::WFF = S[n]

# ╔═╡ 35bb93bc-446a-11eb-27fa-f5f8c7ca2bb4
"""
A data structure defining sequents.

It contains the set of non-logical axioms `S`,
a propositional formula `t`,
a rule (logical axiom, rule of inference, or non-logical axiom) used to derive the proposition,
a list of arguments passed to the rule function.

`S ⊢ t`
"""
struct Sequent
    S::Vector{WFF}
    t::WFF
    rule::Function
    args::Vector{Any}

    #checks if the sequent rule gives the required formula
    Sequent(S, t, rule, args) = rule != MP && rule(args...) != t ?
        error("Inconsistent sequent") :
        new(S, t, rule, args)
end

# ╔═╡ 524e78dc-446a-11eb-0d45-e324018633a2
#The data-type for formal proofs
FormalProof = Vector{Sequent}  
#a FormalProof here is implemented as a list of Sequents

# ╔═╡ 60b80942-446a-11eb-1f56-2d2a114cace1
"""
A function that takes in a FormalProof (a list of sequents),
and verifies if the steps are correct.

If the formal proof is correct, the last propositional formula is returned.
"""
function verify(FP::FormalProof)
	
    for (i, Seq) ∈ enumerate(FP)
        if Seq.rule == MP
            m, n = Seq.args[begin]
			
			#The indexing here is relative
			#The numbers `m` and `n` indicate the number of steps one has to go back to find the relevant statements in the formal proof
            MP(FP[i - m].t, FP[i - n].t) == Seq.t ?
            println("Step $i involving MP is correct.") :
            error("Error in step $i in the formal proof.")
        end
    end

    println("\n The formal proof is correct!")

    return FP[end].t
end

# ╔═╡ 6fd1c7d6-446a-11eb-25c9-2d09cf7cdfa7
md"Demonstration of formal proof verification"

# ╔═╡ 7fcaa95c-446a-11eb-3c8f-91f26a863e7d
#A demonstration of formal verification of the proof of propositional formula `p ⟹ r`.

let S = [:(p ⟹ q), :(q ⟹ r)]
    proof::FormalProof = [
		
    Sequent(S, :(q ⟹ r), NLA, [S, 2]),
		
    Sequent(S, :(p ⟹ q), NLA, [S, 1]),
		
    Sequent(S, :((q ⟹ r) ⟹ (p ⟹ (q ⟹ r))), LA₁, [:(q ⟹ r), :(p)]),
		
    Sequent(S, :((p ⟹ (q ⟹ r))), MP, [(1, 3)]),
		
    Sequent(S, :( (p ⟹ (q ⟹ r)) ⟹ ((p ⟹ q) ⟹ (p ⟹ r)) ), LA₂, [:p, :q, :r]),
		
    Sequent(S, :((p ⟹ q) ⟹ (p ⟹ r)), MP, [(1, 2)]),
		
    Sequent(S, :(p ⟹ r), MP, [(1, 5)])
    
	]

    verify(proof)
end

# ╔═╡ cb7cfae4-446a-11eb-20f1-17803847b3c9
md"---"

# ╔═╡ 11e53870-446b-11eb-30f6-7970dfa209ff
md"---"

# ╔═╡ 32e43864-446b-11eb-2cf6-9f4c4f344de0
md"#### Theorem prover for propositional formulae"

# ╔═╡ f3ccc252-45ca-11eb-04be-bfd1d88f8d7d
md"
The theorem prover is designed to prove tautologies, i.e, sequents of the type $\vdash t$.

This is sufficient because of the Deduction theorem. Proving a sequent of the form $\{s\} \vdash t$ is equivalent to proving the tautology $\vdash (s \to t)$.
"

# ╔═╡ 03d01b40-45cb-11eb-0653-fb376c4632df
md"Also, since proofs are necessarily finite, any proof can make use of only finitely many logical axioms.

One can fix a set $T$ of propositions derived from the logical axioms, and ask whether a given proposition can be derived using only the propositions in $T$."

# ╔═╡ 52f6a038-446b-11eb-280b-37feb64683ff
"""
Recurses through the expression `expr`, searching for an occurrence of the expression `pf` on the right of an implication sign.

The output is a vector of terms that were on the left side of an implication sign.

**Syntax:** `nested(expr::WFF, pf::WFF)::Vector{<:Any}`
"""
function nested(expr::WFF, pf::WFF)
    if expr == pf
        return [pf]
    end
	
    if typeof(expr) != Symbol && expr.args[begin] == :⟹
		#expr = a ⟹ b
        a, b = expr.args[end-1:end]

        return [a; nested(b, pf)]
    end
    
	return [expr]
end

# ╔═╡ b1f64a96-45c9-11eb-0a15-59dada9e6a1d
#A struct for tautologies, i.e, formulae that can be derived by applying logical axioms and using Modus Ponens
begin 
	struct Tautology
		formula::WFF
		formalproof::Vector{Sequent}
	end
	
	#A function that makes initialisation of objects convenient
	Tautology(rule::Function, args::Vector{<:Any}) = Tautology(rule(args...),[Sequent(WFF[], rule(args...), rule, args)])
end

# ╔═╡ 6481460a-446b-11eb-390e-bbf4a771dc99
"""
Returns a set of tautologies obtained by plugging the elements of the input set `S` into to the logical axioms.
"""
function τ(S::Vector{<:Any})::Vector{Tautology}
    T = Tautology[]

    append!(T, Tautology.(LA₁, [[s, t] for s ∈ S for t ∈ S]))
    append!(T, Tautology.(LA₂, [[s, t, u] for s ∈ S for t ∈ S for u ∈ S]))
    append!(T, Tautology.(LA₃, [[s] for s ∈ S]))
    append!(T, Tautology.(LA₄, [[s, t] for s ∈ S for t ∈ S]))

    T
end

# ╔═╡ 1760bfac-45cb-11eb-1daf-b157a0836457
md"##### Outline of the algorithm of the `proof_of` function

1. Let $\varphi$ be the propositional formula whose proof is to be determined from a set $T$ of tautologies.

2. A propositional formula $p$ is called a _$\varphi$-proof candidate_ if it is of the form
$p_1 \to p_2 \to \ldots \to p_k \to \varphi$, where $p_1, \ldots, p_k$ are propositional formulae.

3. The propositional formula $\varphi$ can be proved from $T$ is each of the propositions $p_1, p_2, \ldots, p_k$ can be proved from $T$.

4. To check whether the propositions $p_1, p_2, \ldots, p_k$ can be proved from $T$, one can call the function `proof_of(p_1)`, `proof_of(p_2)`, ..., `proof_of(p_k)`. 

5. If the proofs are all non-empty (if proofs exist for all these formulae), then one can apply Modus Ponens repeatedly to construct a proof for $\varphi$."

# ╔═╡ 2591b266-45cb-11eb-3951-e125b6090e5c
md"
##### Pseudo-code for the `proof_of` function

```julia
function proof_of(φ, T)
    ℒ = {ProofCandidate(φ, t) | t ∈ T}
    
    for p ∈ ℒ
        (p₁, p₂, ..., pₖ) = Decomposition(p)
        
        Q₁, Q₂, ..., Qₖ = proof_of(p₁, T), proof_of(p₂, T), ..., proof_of(pₖ, T)
        
        if ¬(Q₁ = ∅) ∧ ¬(Q₂ = ∅) ∧ ... ∧ ¬(Qₖ = ∅)
            return ModusPonens(Q₁, Q₂, ..., Qₖ)
        else
            continue
        end
    end
    
    return ∅
end 
```
"

# ╔═╡ a207fc46-45cc-11eb-3531-a3c970cf654c
"""
Produces new theorems from a given set `T` of tautologies by applying Modus Ponens wherever applicable.

Additional parameters offer more control on the output of the function.

`iter` - the number of iterations.
`cutoff` - apply Modus Ponens only when atleast one of the terms involved has a position in the list greater than this number
`cap` - the upper limit on the number of theorems that can be contained in the list.
"""
function producetheorems(T::Vector{Tautology}; iter::Int = 1, cutoff::Int = 0, cap::Int = 100000)
    if iter == 0
        return T
    end
    
	#the list of all new theorems derived from the tautologies
    theorems = Tautology[]
    
    for (i, s) ∈ enumerate(T), (j, t) ∈ enumerate(T)
		if length(T) + length(theorems) > cap
			return [T; theorems]
		end 
		
        if (i > cutoff) && (j > cutoff)		
            
			f = s.formula.args
			
            if f[begin] == :⟹ && (f[begin+1] == t.formula)
				
				#creates a new tautology, along with a formal proof of it
                push!(theorems, 
                Tautology(f[end], 
                [t.formalproof; s.formalproof; 
                Sequent(WFF[], f[end], MP, [(1, length(s.formalproof)+1)])]
						))
            end
			
        end
    end

    return producetheorems([T; theorems]; iter = iter-1, cutoff = length(T), cap = cap)
end

# ╔═╡ f2665ef0-45fe-11eb-174a-1d07849323f1
"""
Searches for the given proposition in the list of `theorems`, and returns it if found. Otherwise, the boolean `false` is returned.

**Syntax:** `search_proof(prop::Expr, theorems::Vector{Tautology})`
"""
function search_proof(prop::Expr, theorems::Vector{Tautology})
    for theorem ∈ theorems
        if theorem.formula == prop
            return theorem
        end
    end

    return false
end

# ╔═╡ 7120f5f4-446b-11eb-2d52-f1b2bd4760d2
"""
Returns a formal proof (list of sequents) of a propositional formula `prop`, using tautologies specified in the list `T`.

The output is an empty list is no proof can be produced using the tautologies in `T`.
"""
function prove(ψ::Expr, T::Vector{Tautology}, L::Tuple{Vararg{Symbol}} = 𝕃; cap = 100000, depth = 3)::Tautology
	#the list of all theorems
	𝒯 = producetheorems(T; cap = cap)
	
	function proof_of(φ::Expr, c = 0)
		pr = search_proof(φ, 𝒯)
		if pr != false
			return pr
		end
		
		#the main list in which a potential proof is written
		φproof = Sequent[]
		
		if c < depth
		
			ℒ = [(t, nested(t.formula, φ)) for t ∈ T]

			#removes formulae which
			#-are not φ-proof candidates
			#-are circular, i.e., require a proof of φ to prove φ
			#-are not tautologies
			filter!((t, n)::Tuple{Tautology, Vector{<:Any}} -> 
				n[end] == φ && φ ∉ n[begin:end-1] && length(ν(n, L)) == 2^length(L), ℒ)


			for (t, n) ∈ ℒ
				append!(φproof, t.formalproof)

				for f ∈ n[begin:end-1]
					prf = proof_of(f, c+1).formalproof

					if length(prf) > 0
						append!(φproof, prf)
						push!(φproof, 
							Sequent(WFF[], φproof[end-length(prf)].t.args[end], MP, [(1+length(prf), 1)]))
					else
						φproof = Sequent[]
						break
					end
				end
				
				length(φproof) > 0 ? break : continue
			end
		end
		
		theorem = Tautology(φ, φproof)
		push!(𝒯, theorem)
		return theorem				
	end
	
	
	proof_of(ψ)
end

# ╔═╡ 5e0aff9c-45cd-11eb-0c2a-510ca74bb2f4
let T₀ = τ(S𝕃([:s], 2, :⟹))
	prove(:(s ⟹ s), T₀, (:s, ); cap = 3_000)
end

# ╔═╡ 85b29a80-46dd-11eb-2163-5dbae38cc04d
md"---"

# ╔═╡ Cell order:
# ╟─3f153a28-445e-11eb-2e98-d115965e9c71
# ╟─602b6b24-445e-11eb-30f1-4f49734960a3
# ╟─9fb63616-445e-11eb-214e-3db3daaa4284
# ╟─247443de-445f-11eb-2e6d-a5bfa9851737
# ╟─777b58d8-445f-11eb-2b47-fdd4daefea79
# ╟─fd470142-445f-11eb-05a5-d7cb6c10720a
# ╟─2af3b9bc-4462-11eb-0f9d-f3e4ed1e988a
# ╟─13a5b0f6-4462-11eb-3743-81e88a95eb29
# ╟─172f7430-445f-11eb-23e6-97e4eb96db51
# ╠═25213860-445e-11eb-01b6-0d566af125ae
# ╠═8892e196-445e-11eb-3113-d1815e252604
# ╠═f6ffa9ac-445e-11eb-152f-4dbaaad9e32a
# ╠═ffd5f324-445e-11eb-1dd8-0dd2a846a650
# ╠═06093198-445f-11eb-30a2-a3c4d3c271ac
# ╟─45791c6c-445f-11eb-2db2-f1567a1b9fd4
# ╟─2d60d5a6-4460-11eb-1dc0-6b0d659c697f
# ╠═32c7a836-445f-11eb-031c-9775d9466750
# ╟─4f542740-445f-11eb-3358-1f8684540652
# ╟─7946e0d0-45b8-11eb-3d85-3d2bc8493565
# ╟─5309d4ca-445f-11eb-344c-ff6dfba11977
# ╠═66cf1092-445f-11eb-0bf7-89ab73d35dc6
# ╠═3422d740-4460-11eb-0653-dd60c027d677
# ╠═1e309c6a-4460-11eb-1542-c96357ecf472
# ╠═7fd80674-4460-11eb-11d9-83146bf540ee
# ╟─be8dbd78-4460-11eb-1641-65b1cf8113af
# ╟─d5b649ca-4460-11eb-1483-0b359ee64cba
# ╠═e8f8ca58-4460-11eb-09b4-e91a325e1863
# ╠═14732d9a-4461-11eb-3a59-85e4949a755c
# ╠═6a16b064-4461-11eb-09be-6f8fd0c921e5
# ╠═1e8e6354-45b7-11eb-2260-e36742dd8de2
# ╠═7413d074-4461-11eb-3949-23bc47825926
# ╠═83a78aba-4461-11eb-107b-d75a1826ffb2
# ╠═9bbe2b62-4461-11eb-35d4-99da63f0d6b6
# ╟─aa0177fa-45fa-11eb-0ca1-07ab18c65573
# ╠═a35c287c-4461-11eb-0009-fbfbe05230e2
# ╠═ac833102-4461-11eb-3bb6-cbdf7573e7b1
# ╠═bc523d9e-4461-11eb-25b7-379ebdb6a94f
# ╠═f45dda24-4461-11eb-3efa-fd40641b886e
# ╟─025ecf2a-4462-11eb-3b21-a5503782a1a9
# ╟─369ba810-4462-11eb-184d-271abf7bdff0
# ╟─3b49f416-4462-11eb-0af9-0be138384abf
# ╟─6e53519a-4462-11eb-0b28-fdf3e3e2f92e
# ╟─d8b3f38e-4462-11eb-3e99-9b3d9a71e36a
# ╠═7edba51a-4462-11eb-16f7-ab29e537faac
# ╟─7ac112fe-4468-11eb-17c9-c7f945429086
# ╠═b8889130-4462-11eb-1fdf-433cb2015f17
# ╟─315ca820-4469-11eb-23f4-37a7ecac25bf
# ╠═fa99e56a-4469-11eb-1a4b-5dd6844513c2
# ╟─35bb93bc-446a-11eb-27fa-f5f8c7ca2bb4
# ╠═524e78dc-446a-11eb-0d45-e324018633a2
# ╠═60b80942-446a-11eb-1f56-2d2a114cace1
# ╟─6fd1c7d6-446a-11eb-25c9-2d09cf7cdfa7
# ╠═7fcaa95c-446a-11eb-3c8f-91f26a863e7d
# ╟─cb7cfae4-446a-11eb-20f1-17803847b3c9
# ╟─11e53870-446b-11eb-30f6-7970dfa209ff
# ╟─32e43864-446b-11eb-2cf6-9f4c4f344de0
# ╟─f3ccc252-45ca-11eb-04be-bfd1d88f8d7d
# ╟─03d01b40-45cb-11eb-0653-fb376c4632df
# ╠═52f6a038-446b-11eb-280b-37feb64683ff
# ╠═b1f64a96-45c9-11eb-0a15-59dada9e6a1d
# ╠═6481460a-446b-11eb-390e-bbf4a771dc99
# ╟─1760bfac-45cb-11eb-1daf-b157a0836457
# ╟─2591b266-45cb-11eb-3951-e125b6090e5c
# ╠═a207fc46-45cc-11eb-3531-a3c970cf654c
# ╠═f2665ef0-45fe-11eb-174a-1d07849323f1
# ╠═5e0aff9c-45cd-11eb-0c2a-510ca74bb2f4
# ╠═7120f5f4-446b-11eb-2d52-f1b2bd4760d2
# ╟─85b29a80-46dd-11eb-2163-5dbae38cc04d
