# Propositional Logic

This is an implementation of the rules of propositional logic in the [Julia language](https://julialang.org/). The content is based on Dr. Amit Kuber's lectures on Propositional Logic during [ISLA 2020](https://sites.google.com/iiserb.ac.in/isla-2020-virtual/). A recording of a presentation of the contents of this program is available on YouTube at [this](https://youtu.be/7KBuwBp2Pd8?t=7614) link.

Propositional formulae are implemented as Julia expressions, and several operations like valuation and checking equivalence are defined. Further, `Sequent`s are implemented as Julia structs and formal proofs are defined as lists of `Sequent`s. It is possible to write out formal proofs of propositional formulas and check their validity step-by-step. A short demonstration of proof verification is included.

---
#### Hilbert-style deductive calculus

__*The Logical Axioms*__

**LA1.**     $`s \to (t \to s)`$

**LA2.**     $`(s \to (t \to u)) \to ((s \to t) \to (s \to u))`$

**LA3.**     $`\neg \neg s \to s`$

**LA4.**     $`(\neg s \to \neg t) \to (t \to s)`$

__*Rule of Inference -* Modus Ponens__

$`s \to t`$

$`s`$

---

$`t`$

__*Sequents*__

For $`S \subseteq S\scr{L}`$, and $`s, t \in S\scr{L}`$

**(NLA).**  If $`s \in S`$ then $`S\vdash s`$.

**(LA).** If $`t`$ is a logical axiom then $`S \vdash t`$.

**(MP).** If $`S \vdash (s \to t)`$ and $`S \vdash s`$ are valid sequents then $`S \vdash t`$.

---


The next section of the code contains an attempt at using the logical axioms of the Hilbert-style deductive calculus to produce new theorems. The main function used here is the `proof_of` function, and the details of this function are described below. 

The theorem prover is designed to prove tautologies, i.e, sequents of the type $\vdash t$.

This is sufficient because of the Deduction theorem. Proving a sequent of the form $`\{s\} \vdash t`$ is equivalent to proving the tautology $`\vdash (s \to t)`$. 

Also, since proofs are necessarily finite, any proof can make use of only finitely many logical axioms.

One can fix a set $`T`$ of propositions derived from the logical axioms, and ask whether a given proposition can be derived using only the propositions in $`T`$.

This is done in the `proof_of` function. Here is a brief outline of the algorithm:

1. Let $`\varphi`$ be the propositional formula whose proof is to be determined from a set $`T`$ of tautologies.

2. A propositional formula $`p`$ is called a _$`\varphi`$-proof candidate_ if it is of the form
$`p_1 \to p_2 \to \ldots \to p_k \to \varphi`$, where $`p_1, \ldots, p_k`$ are propositional formulae.

3. The propositional formula $`\varphi`$ can be proved from $`T`$ is each of the propositions $`p_1, p_2, \ldots, p_k`$ can be proved from $`T`$.

4. To check whether the propositions $`p_1, p_2, \ldots, p_k`$ can be proved from $`T`$, one can call the function `proof_of(p_1)`, `proof_of(p_2)`, ..., `proof_of(p_k)`. 

5. If the proofs are all non-empty (if proofs exist for all these formulae), then one can apply Modus Ponens repeatedly to construct a proof for $\varphi$.


And here is the pseudo-code:

```julia
function proof_of(φ, T)
    ℒ = {ProofCandidate(φ, t) | t ∈ T}
    
    for p ∈ ℒ
        (p₁, p₂, ..., pₖ) = Decomposition(p)
        
        Q₁, Q₂, ..., Qₖ = proof_of(p₁, T), proof_of(p₂, T), ..., proof_of(pₖ, T)
        
        if ¬(Q₁ = ∅) ∧ ¬(Q₂ = ∅) ∧ ... ∧ ¬(Qₖ = ∅)
            return ModusPonens(Q₁, Q₂, ..., Qₖ)
        else
            continue
        end
    end
    
    return ∅
end 
```

The recursive calls may be to many when the input set `T` is large, which is why combining this algorithm with a "theorem library" (a collection of tautologies deduced from the given set of statements `T` using Modus Ponens) may reduce the computational time significantly.

The file `propositional_logic_presentation.jl` contains the most up-to-date version of the code, and may be viewed either on a regular code editor or on a [Pluto notebook](https://juliapackages.com/p/pluto).
